#!/usr/bin/env python
# _*_ coding:utf-8 _*_
"""
@Version: python3.8
@Project: Excels
@File:    sheetB.py
@Date:    2022/11/9 22:04
@Author:  jia
@Desc:    excel表格练习
"""

import xlwings as xw
# app = xw.App(visible=True, add_book=False)
# book = app.books.add()
# sht = book.sheets.add(name='表格1')

# 方法2，新增一个表格
sht = xw.Book().sheets('sheet1')
sht.name = '表格1'
# 插入数据
# sht.range((1,2),(1,7)).value = ['数学','语文','英语','化学','生物','历史']
sht.range('b1:g1').value = ['数学', '语文', '英语', '化学', '生物', '历史']
sht.range('b2:g2').value = list(range(1000, 7000, 1000))

# 写入一列 transpose(调换）
sht.range('A2').options(transpose=True).value = ['王者荣耀', '荒野求生', '小王', '小白']
sht.range('a8').value = [['游戏111111'], ['it22222'], ['物联网3333'], ['金融4444']]

# 工作表 自动调整宽，高
sht.autofit(axis='c')        # 列的宽度
sht.autofit(axis='r')        # 行的宽度

# 获取最大行数，和列数
rows = sht.used_range.last_cell.row
columns = sht.used_range.last_cell.column

# 画表格框线
sht.range((1, 1), (rows, columns)).api.Borders(8).LineStyle = 1  # 上边框
sht.range((1, 1), (rows, columns)).api.Borders(9).LineStyle = 1  # 下边框
sht.range((1, 1), (rows, columns)).api.Borders(7).LineStyle = 1  # 左边框
sht.range((1, 1), (rows, columns)).api.Borders(10).LineStyle = 1  # 右边框
sht.range((1, 1), (rows, columns)).api.Borders(12).LineStyle = 1  # 内横边框
sht.range((1, 1), (rows, columns)).api.Borders(11).LineStyle = 1  # 内纵边框
