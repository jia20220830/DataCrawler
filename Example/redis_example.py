# -*- coding:UTF-8 -*-
"""
@Project:   DataCrawler
@FileName:  redis_example.py
@CreateDate:2023/4/28 10:12  
@Author:    Jia  
@Desc:      redis用法示例
"""
from Config import log
from database import redis_db
logger = log.Logs().debug_logger()
# 使用redis db0来连接
r = redis_db.redis_conn(logger)

"""字符串"""

"""列表"""
# 从列表左侧添加元素
r.lpush("score", 10, 20, 30, 40)
# 从列表右侧添加元素
r.rpush("age", 10, 20, 30, 40)
# 输出
print(r.lrange("age", 0, -1)) # [10,20,30,40]
# 重新赋值
r.lset("score", 0, 521)
# 取出索引号是0的值
print(r.lindex("list2", 0))
# 将列表中所有的"22"删除
r.lrem("list2", "22", 0)
# 将列表中左边第一次出现的"11"删除
r.lrem("list2", "11", 1)
# 将列表中右边第一次出现的"99"删除
r.lrem("list2", "99", -1)


"""哈希"""
r.hset("xiaobai", "name", "guo")
r.hset("xiaobai", "age", 28)
# 取所有key
print(r.hkeys("xiaobai"))  # ['name', 'age']
# 取所有的value
print(r.hvals("xiaobai"))  # ['guo', '28']
# 取name的value
print(r.hget("xiaobai", "name"))  # guo
# 取多个value
print(r.hmget("xiaobai", "name", "age"))  # ['guo', '28']
# 批量添加
r.hmset("hash1", {"k1": "v1", "k2": "v2", "k3": "v3"})
# 取多个key的value
print(r.hmget("hash1", "k1", "k2", "k3"))  # ['v1', 'v2', 'v3']
# 取出所有键值 对
print(r.hgetall("xiaobai"))  # {'name': 'guo', 'age': '28'}
# 删除指定键值 对
r.hdel("hash1", "k1")
# 自增 自减 amount - 自增数（整数，负数表示自减）
r.hincrby('count', 'num', amount=1)

"""集合"""
# 新增
r.sadd('set1', 1, 2, 3, 4, 5)
# 获取元素个数
r.scard('set1')
# 获取所有元素
r.smembers('set1')
# 所有成员(元组形式)
r.sscan('set1')

# 所有成员(迭代器的方式)
for item in r.sscan_iter('set1'):
    print(item)
    # 输出结果如下：
    # 1
    # 2
    # 3

# 是否在集合中  #返回True表示在集合中
r.sismember('set1', 3)
# 将元素从一个集合移动到另一个集合
r.smove('set1', 'set2', 4)
# 删除一个元素
r.srem('set1', 2)
# 随机删除一个元素并返回
r.spop('set1')


"""有序集合"""

