# -*- coding:UTF-8 -*- 
"""
@Project:   DataCrawler
@FileName:  output.py
@CreateDate:2023/4/24 21:57  
@Author:    Jia  
@Desc:      ->输出数据并保存
"""


class Output:
    def __init__(self, rds):
        self.rds = rds

    def collect_data(self, data):
        """集合所有解析的数据"""

        if data is None:
            return None
        else:
            pass

    def export_html(self):
        """以HTML的格式输出爬取数据"""

        keys = self.rds.hkeys('first_floor_data')
        if len(keys) == 0 or None:
            return None
        else:

            f = open('./Export/first_export.html', 'w', encoding='utf-8')
            f.write("<html>")

            f.write("<head>")
            f.write('<meta http-equiv="Content-Type" content="text/html;charset=utf-8">')
            f.write("<title>第一层数据</title>")
            f.write('<style type="text/css"> ')
            f.write('body {background-color: AliceBlue}')
            f.write('.title {color:red; text-align:center}')
            f.write('</style>')
            f.write("</head>")

            f.write("<body>")
            f.write('<h1 class="title" >第一层数据</h1>')
            f.write('<h2 style="color:black"><pre>公司名                         URL</pre></h2>')

            f.write("<table>")
            for key in keys:
                f.write("<tr>")
                f.write("<td>%s</td>" % key)
                f.write("<td>%s</td>" % self.rds.hget('first_floor_data', key))
                f.write("</tr>")
            f.write("</table>")

            f.write("</body>")

            f.write("</html>")
            f.close()

    def third_export_html(self):
        """以HTML的格式输出爬取数据"""

        keys = self.rds.hkeys('third_floor_data')
        if len(keys) == 0 or None:
            return None
        else:

            f = open('../Export/third_export.html', 'w', encoding='utf-8')
            f.write("<html>")

            f.write("<head>")
            f.write('<meta http-equiv="Content-Type" content="text/html;charset=utf-8">')
            f.write("<title>第三层数据</title>")
            f.write('<style type="text/css"> ')
            f.write('body {background-color: AliceBlue}')
            f.write('.title {color:red; text-align:center}')
            f.write('</style>')
            f.write("</head>")

            f.write("<body>")
            f.write('<h1 class="title" >第三层数据</h1>')
            f.write('<h2 style="color:black"><pre>姓名    电话</pre></h2>')

            f.write("<table>")
            for key in keys:
                f.write("<tr>")
                f.write("<td>%s</td>" % key)
                f.write("<td>%s</td>" % self.rds.hget('third_floor_data', key))
                f.write("</tr>")
            f.write("</table>")

            f.write("</body>")

            f.write("</html>")
            f.close()


if __name__ == '__main__':
    from Config.log import Logs
    from database import redis_db
    logger = Logs().debug_logger()
    rds = redis_db.redis_conn2(logger)
    out = Output(rds)
    # out.export_html()
    out.third_export_html()
