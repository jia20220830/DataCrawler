#! /usr/bin/env python3
# -*- coding:UTF-8 -*-
"""
@Project:   DataCrawler
@FileName:  print_dir.py
@Create:    2023/5/7 14:19
@Version:   Python3.9  
@Author:    Jia  
@Descr:     ->显示目录树状图
"""
import os


def generate_file_tree_local(colora, colorb, path: str, depth: int, site: list, ignore: list):
    """
    递归打印文件目录树状图（使用局部变量）

    :param colora: \033[1m开始格式输出符  \033[34m显示的颜色 eg: \033[1m\033[34mHello, world!\033[0m
    :param colorb: \033[0m 结束格式输出符
    :param ignore: 不显示的文件夹
    :param path: 根目录路径
    :param depth: 文件所在的层级号，一级目录是0，二级目录是1
    :param site: 存储出现转折的层级号
    :return: None
    """
    void_num = 0
    filenames_list = os.listdir(path)  # 文件夹名称

    # 移除不需要显示的文件夹
    if ignore is None or len(ignore) == 0:
        pass
    else:
        for i_file in ignore:
            if i_file in filenames_list:
                filenames_list.remove(i_file)

    for item in filenames_list:
        string_list = ["│   " for _ in range(depth - void_num - len(site))]
        for s in site:
            string_list.insert(s, "    ")

        if item != filenames_list[-1]:
            string_list.append("├── ")
        else:
            # 本级目录最后一个文件：转折处
            string_list.append("└── ")
            void_num += 1
            # 添加当前已出现转折的层级数
            site.append(depth)
        print("".join(string_list) + colora + item + colorb)

        # 二级目录
        # new_item = path + '/' + item
        # if os.path.isdir(new_item):
        #     generate_file_tree_local('', '', new_item, depth + 1, site, ignore)
        # if item == filenames_list[-1]:
        #     void_num -= 1
        #     # 移除当前已出现转折的层级数
        #     site.pop()


if __name__ == '__main__':
    root_path = r"E:\PycharmProjects\DataCrawler"
    ig = ['.idea', 'crawler_frame.png', 'venv', '__pycache__']
    a = '\033[1m\033[34m'
    b = '\033[0m'
    generate_file_tree_local(a, b, root_path, depth=0, site=[], ignore=ig)
