#!/usr/bin/env python
# _*_ coding:utf-8 _*_
"""
@Version: python3.9
@Project: Excels
@File:    sheetA.py
@Date:    2022/11/4 22:28
@Author:  jia
@Desc:
"""
import xlwings as xw
import time
""" 
创建操作对象
APP>>books>>sheets>>range
app:excel程序
books:工作薄
sheets:表1
range:单元格
visible : 是否在Excel软件中打开文件  
add_book: 是否创建新的工作簿
"""
app = xw.App(visible=True, add_book=False)

# 打开工作簿
book = app.books.open(r'E:\每日日报原版.xls')
# 打开sheet表
sht = book.sheets['sheet1']
b16 = sht.range('b16').value
f16 = sht.range('f16').value
title = b16.replace('-', '.')+' '+f16
print(title)


# 新建一个工作薄
book2 = app.books.add()
# 新建一个工作表
sht2 =book2.sheets['sheet1']


# 设置标题
sht2.range('A1:F1').merge() # 合并单元格
sht2.range('a1').value = '年金组合投管资金到账规模统计\n\n'+title
sht2.range(1,4).row_height = 60 #设置行高
sht2.range('a1').api.VerticalAlignment  # -4108 垂直居中（默认)-4160 靠上，-4107 靠下，-4130 自动换行对齐
sht2.range('a1').api.HorizontalAlignment = -4108 # -4108 水平居中。 -4131 靠左，-4152 靠右。

# 复制表1，粘贴到表2
my_values = sht.range('a1：f16').options(ndim=2).value    # 读取二维的数据
sht2.range('a2').value = my_values
sht2.range('a1:f16').api.Font.Size = 16

# 删除一行单元格
sht2.range('a15').api.EntireRow.Delete()

# 设置全局列宽、行高
sht2.range(2, 1).column_width = 5.5  # 设置（1,4)为第1行第4列的单元格
sht2.range((2,2), (2,5)).column_width = 25
sht2.range(2,6).column_width = 12
sht.range((2,2),(2,6)).row_height = 30 # 设置第2行2列--2行6列行高为30

# 修改单元格内容
sht2.range('b9').value = '职业年金小计'
sht2.range('b10').value = '资金到账合计'

# 设置表头的底色
sht2.range('a2:f2').color = (192,192,192)
sht2.range('a2:f2').api.VerticalAlignment
sht2.range('a2:f2').api.HorizontalAlignment
# 设置第4行底色
sht2.range('a4:f4').color = (217,217,217)
sht2.range('a4:f4').api.VerticalAlignment # 垂直居中
sht2.range('a4:f4').api.HorizontalAlignment = -4131  #左对齐

#清除备注和多余的内容
sht2.range('f16').clear_contents()  #清除内容不清除格式
sht2.range('b16').clear()           # 清除内容和格式

# 表格底部加一行备注
sht2.range('A16:F16').merge()
sht2.range('A17:F17').merge()
sht2.range('A18:F18').merge()
sht2.range('A16:A18').merge()
comment = '备注：1）年金组合投管资金到账规模仅为资金分配金额，不考虑资金提取金额；\n' \
          '     2）年金总规模为投资组合T日净资产规模，包含本日新增到账金额，但估值行情接收尚不全，仅供参考；\n' \
          '     3）当日统计数据以晚间日终清算时间截止，不包含之后到账资金数据。'
sht2.range('a16').value = comment
sht2.range('a16').api.VerticalAlignment # 垂直居中
sht2.range('a16').api.HorizontalAlignment = -4131  #左对齐
sht2.range('a16').api.Font.Size = 10

# 合并一些单元格
sht2.range('a11:b11').merge()
sht2.range('a12:b12').merge()
sht2.range('a13:b13').merge()
sht2.range('a14:b14').merge()
sht2.range('a15:b15').merge()
sht2.range('a11:a15').api.VerticalAlignment           # 重直居中
sht2.range('a11:a15').api.HorizontalAlignment = -4108 #水平居中



# 插入公式
sht2.range('c4').formula = '=sum(c3:c3)'
sht2.range('c9').formula = '=SUM(C5:C8)'
sht2.range('c10').formula = '=C4+C9'
sht2.range('d10').formula = '=D4+D9'
sht2.range('e10').formula = '=E4+E9'
sht2.range('c15').formula = '=SUM(C11:C14)'

# 设置第11行--14行底色
sht2.range((11,1),(14,6)).color = (217,217,217)
sht2.range('a9:f9').color = (217,217,217)
sht2.range('a10:f10').color = (192,192,192)
sht2.range('a15:f15').color = (192,192,192)

# 给表格设置边框
a_range = 'a1:f15'
sht2.range(a_range).api.Borders(8).LineStyle = 1 #上边框
sht2.range(a_range).api.Borders(9).LineStyle = 1 #下边框
sht2.range(a_range).api.Borders(7).LineStyle = 1 #左边框
sht2.range(a_range).api.Borders(10).LineStyle = 1 #右边框
sht2.range(a_range).api.Borders(12).LineStyle = 1 #内横边框
sht2.range(a_range).api.Borders(11).LineStyle = 1 #内纵边框


# 关闭工作薄1
book.close()

# 保存后关闭工作薄2
t = time.gmtime()
today = time.strftime('%Y%m%d', t)
book2.save(fr'E:\每日日报结果{today}.xls')
# book2.close()

# 关闭excel程序
# app.quit()
